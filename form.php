﻿<!DOCTYPE html>
<html>
<head>
	<title> Авторизация </title>
	<style>
		body{
			background-color: whitesmoke;
		}
		.name{
			text-align: center;
			margin: 5%;
			font-size: 300%;
			font-style: italic;
			font-weight: 500;
		}
		form{
			margin: 5%;
			text-align: center;
			font-size: 150%;
			font-style: italic;
		}
		.login{
			display: inline;
			margin: 2%; 
		}
		.input_login{
			display: inline;
			margin: 2%;
		}
		.password{
			display: inline;
			margin: 1.7%;
		}
		.input_password{
			display: inline;
			margin: 1.7%;
		}
		.submit{
			margin: 4%;
		}
		input{
			width: 300px;
			font-size: 16px;
			padding: 6px 0 4px 10px;
			border: 1px solid #ebc2bf;
			background: #FFFFFF;
			border-radius: 8px;
		}
	</style>
</head>
<body>
	<div class="name"><img src="img/Авторизация.png"></div>
	<form method="post">
		<p><div class="login">Логин</div><div class="input_login"><input type="text" name="login"></p></div>
		<p><div class="password">Пароль</div><div class="input_password"><input type="password" name="password"></p></div>
		<p><div class="submit"><input type="submit" value="Войти" name="submit"></p></div>
		<?php
		if($errors)
		{
			foreach ($errors as $error) 
			{
				echo "<div style=\"font-size:30px;text-align: center;color: #DC143C\"><p class=\"err\">$error</p></div>";
			}
		}
		if($r)
		{
			echo '<div style="font-size:30px;text-align: center;color: #DC143C">Вы успешно авторизованы</div>';
		}
		?>
	</form>
</body>
</html>